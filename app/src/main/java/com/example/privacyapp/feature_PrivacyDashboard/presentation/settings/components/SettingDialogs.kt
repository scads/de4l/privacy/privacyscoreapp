package com.example.privacyapp.feature_PrivacyDashboard.presentation.settings.components

import android.app.ActivityManager
import android.content.Context
import androidx.compose.runtime.Composable
import com.example.privacyapp.feature_PrivacyDashboard.domain.location.LocationService
import com.example.privacyapp.feature_PrivacyDashboard.domain.util.ApplicationProvider
import com.example.privacyapp.feature_PrivacyDashboard.presentation.coreComponents.InfoDialog
import com.example.privacyapp.feature_PrivacyDashboard.presentation.settings.SettingsScreenEvent
import com.example.privacyapp.feature_PrivacyDashboard.presentation.settings.SettingsScreenViewModel

/**
 * Composable function that displays a dialog when the tracking interval is changed, providing information and instructions.
 *
 * @param viewModel The ViewModel instance managing the settings state and logic.
 */
@Composable
fun ShowTrackingIntervalChangedDialog(viewModel: SettingsScreenViewModel) {
    if (ApplicationProvider.application.isServiceRunning(LocationService::class.java)) {
        InfoDialog(
            infoText = "Successfully Saved! \n" +
                    "Please restart the tracking manually from the dashboard to apply the new location tracking interval!",
            onDismiss = {
                viewModel.onEvent(SettingsScreenEvent.ToggleValuesSaved)
                viewModel.onEvent(SettingsScreenEvent.ToggleTrackingIntervalChanged)
            }
        )
    } else {
        viewModel.onEvent(SettingsScreenEvent.ToggleValuesSaved)
        viewModel.onEvent(SettingsScreenEvent.ToggleTrackingIntervalChanged)
    }
}

/**
 * Composable function that displays a dialog when the POI settings are changed, allowing the user to confirm recomputing POIs.
 *
 * @param viewModel The ViewModel instance managing the settings state and logic.
 */
@Composable
fun ShowPOISettingsChangedDialog(viewModel: SettingsScreenViewModel) {
    POIChangeDialog(
        onDismiss = {
            viewModel.onEvent(SettingsScreenEvent.ToggleValuesSaved)
            viewModel.onEvent(SettingsScreenEvent.TogglePOISettingsChanged)
        },
        onConfirm = {
            viewModel.onEvent(SettingsScreenEvent.RecomputePOIsWithNewParameters)
            viewModel.onEvent(SettingsScreenEvent.ToggleValuesSaved)
            viewModel.onEvent(SettingsScreenEvent.TogglePOISettingsChanged)
        }
    )
}

/**
 * Checks if a specified service is currently running in the background.
 *
 * @param service The class of the service to check.
 * @return `true` if the service is running, `false` otherwise.
 */
@Suppress("DEPRECATION") // Deprecated for third party Services.
private fun Context.isServiceRunning(service: Class<LocationService>) =
    (getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager)
        .getRunningServices(Integer.MAX_VALUE)
        .any { it.service.className == service.name }