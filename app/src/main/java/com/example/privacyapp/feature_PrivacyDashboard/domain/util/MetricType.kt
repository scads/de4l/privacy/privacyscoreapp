package com.example.privacyapp.feature_PrivacyDashboard.domain.util

/**
 * Represents different types of metrics.
 */
enum class MetricType {
    SCORE, ABSOLUT
}