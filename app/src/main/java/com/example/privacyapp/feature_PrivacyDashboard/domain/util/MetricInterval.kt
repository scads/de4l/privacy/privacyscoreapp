package com.example.privacyapp.feature_PrivacyDashboard.domain.util

/**
 * Represents different intervals for metrics.
 */
enum class MetricInterval {
    DAY, WEEK, MONTH
}