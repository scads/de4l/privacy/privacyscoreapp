package com.example.privacyapp.feature_PrivacyDashboard.presentation.dashboard


import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.provider.Settings
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutVertically
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material.icons.filled.Info
import androidx.compose.material.icons.filled.Refresh
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Switch
import androidx.compose.material3.SwitchDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.core.app.ActivityCompat.shouldShowRequestPermissionRationale
import androidx.core.content.ContextCompat
import androidx.navigation.NavController
import com.example.privacyapp.feature_PrivacyDashboard.domain.location.LocationService
import com.example.privacyapp.feature_PrivacyDashboard.domain.util.ApplicationProvider
import com.example.privacyapp.feature_PrivacyDashboard.presentation.MainActivity
import com.example.privacyapp.feature_PrivacyDashboard.presentation.coreComponents.AppItem
import com.example.privacyapp.feature_PrivacyDashboard.presentation.coreComponents.InfoDialog
import com.example.privacyapp.feature_PrivacyDashboard.presentation.coreComponents.LineChartV2
import com.example.privacyapp.feature_PrivacyDashboard.presentation.coreComponents.LineChartV2YAxisRight
import com.example.privacyapp.feature_PrivacyDashboard.presentation.dashboard.components.BackgroundLocationPermissionTextProvider
import com.example.privacyapp.feature_PrivacyDashboard.presentation.dashboard.components.LocationPermissionTextProvider
import com.example.privacyapp.feature_PrivacyDashboard.presentation.dashboard.components.MetricSection
import com.example.privacyapp.feature_PrivacyDashboard.presentation.dashboard.components.NotificationPermissionTextProvider
import com.example.privacyapp.feature_PrivacyDashboard.presentation.dashboard.components.PermissionDialog
import com.example.privacyapp.feature_PrivacyDashboard.presentation.util.NavigationItem


/**
 * Composable function to display the main dashboard screen.
 *
 * @param navController The NavController to handle navigation.
 * @param viewModel The ViewModel containing the dashboard data and logic.
 * @param mainActivity The MainActivity instance.
 */
@Composable
fun DashboardScreen(
    navController: NavController,
    viewModel: DashboardViewModel,
    mainActivity: MainActivity
) {

    val mContext = LocalContext.current

    val dialogQueue = viewModel.visiblePermissionDialogQueue
    //Request Permissions
    val permissionsToRequest = if (Build.VERSION.SDK_INT >= 33) {
        arrayOf(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_BACKGROUND_LOCATION,
            Manifest.permission.POST_NOTIFICATIONS
        )
    } else if (Build.VERSION.SDK_INT >= 30) {
        arrayOf(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_BACKGROUND_LOCATION
        )
    } else {
        arrayOf(
            Manifest.permission.ACCESS_FINE_LOCATION
        )
    }

    val multiplePermissionResultLauncher = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.RequestMultiplePermissions(),
        onResult = { _ ->
            permissionsToRequest.forEach { permission ->
                viewModel.onPermissionResult(
                    permission = permission,
                    mainActivity = mainActivity
                )
            }
        }
    )

    val scrollState = rememberScrollState()

    Column {
        if (viewModel.energySaverDialogVisible.value) {
            AlertDialog(
                onDismissRequest = {
                    viewModel.onEvent(DashboardEvent.DismissEnergyDialog)
                },
                confirmButton = {
                    Button(onClick = {
                        val intent = Intent(
                            Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS,
                            Uri.fromParts(
                                "package",
                                ApplicationProvider.application.packageName,
                                null
                            )
                        )
                        mContext.startActivity(intent)
                        viewModel.onEvent(DashboardEvent.DismissEnergyDialog)
                    }) {
                        Text(text = "OK")
                    }
                },
                title = {
                    Text(text = "IMPORTANT")
                },
                text = {
                    Text(text = "Please turn of the Battery Optimizations for this APP, otherwise it cant be guaranteed that this app works as intended.\n If the Ok Button does not work for you, please go into the App-settings manually and disable battery Optimizations.")
                },
                dismissButton = {
                    Button(onClick = { viewModel.onEvent(DashboardEvent.DismissEnergyDialog) }) {
                        Text(text = "No")
                    }
                }
            )
        }

        if (viewModel.infoDialogVisible.value) {
            val dashboardExplanation = """
    **Edit Configuration:**
    At the top, an "Edit" button allows you to customize settings. Adjust the time interval and metrics used, including the option to select multiple metrics. These changes directly influence the dashboard's diagram.
    
    **Metrics Explanation:**
    The metrics "Stop Detection" assesses the number of identified Points of Interest (POIs), while "Stop Frequency" gauges occurrences of long-term POIs, ones that appear multiple times.
    
    **Detailed Metric Settings:**
    In the Settings screen, you can further tailor these metrics to your preferences.
    
    **Dashboard Diagram:**
    The chart displays two lines:
    - Green Line: Represents absolute values of data points.
    - Blue Line: Depicts a score ranging from 0 to 1.
    
    **Top Impact Apps:**
    Finally, the dashboard showcases the top 5 apps that significantly impacted your usage within the past 24 hours.
""".trimIndent()

            InfoDialog(
                infoText = dashboardExplanation,
                onDismiss = { viewModel.onEvent(DashboardEvent.ToggleInfoDialog) })
        }

        Column {
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically
            ) {
                Box(modifier = Modifier.padding(10.dp, 0.dp, 0.dp, 0.dp)) {
                    Text(
                        text = "Dashboard",
                        style = MaterialTheme.typography.headlineMedium
                    )
                }
                Row {
                    IconButton(
                        onClick = {
                            viewModel.onEvent(DashboardEvent.RefreshData)
                        },
                    ) {
                        Icon(
                            imageVector = Icons.Default.Refresh,
                            contentDescription = "Refresh"
                        )
                    }
                    IconButton(
                        onClick = {
                            viewModel.onEvent(DashboardEvent.ToggleMetricDropDown)
                        },
                    ) {
                        Icon(
                            imageVector = Icons.Default.Edit,
                            contentDescription = "Metric Settings"
                        )
                    }
                    IconButton(
                        onClick = {
                            viewModel.onEvent(DashboardEvent.ToggleInfoDialog)
                        }
                    ) {
                        Icon(imageVector = Icons.Default.Info, contentDescription = "Info")
                    }
                }
            }

            AnimatedVisibility(
                visible = viewModel.metricSectionExpanded.value,
                enter = fadeIn() + slideInVertically(),
                exit = fadeOut() + slideOutVertically()
            ) {
                MetricSection(
                    metrics = viewModel.selectedMetrics.toList(),
                    metricInterval = viewModel.metricInterval.value,
                    onMetricChange = { metric ->
                        viewModel.onEvent(
                            DashboardEvent.ChangeMetric(metric)
                        )
                    },
                    modifier = Modifier.padding(10.dp),
                    onMetricIntervalChange = { metricInterval ->
                        viewModel.onEvent(
                            DashboardEvent.ChangeMetricInterval(
                                metricInterval
                            )
                        )
                    }
                )
            }
        }
        Column(
            modifier = Modifier
                .fillMaxSize()
                .verticalScroll(state = scrollState)
                .padding(0.dp, 0.dp, 0.dp, 10.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {

            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(20.dp, 0.dp, 20.dp, 0.dp),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically
            ) {
                Row(
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically
                ) {

                    Text(text = "Tracking:", style = MaterialTheme.typography.headlineSmall)
                    Spacer(modifier = Modifier.width(20.dp))
                    Switch(
                        checked = viewModel.trackingActive.value,
                        onCheckedChange = { switchOn ->
                            viewModel.onEvent(DashboardEvent.ToggleTracking(switchOn))
                            if (switchOn) {
                                if (ContextCompat.checkSelfPermission(
                                        mainActivity,
                                        Manifest.permission.ACCESS_FINE_LOCATION
                                    ) == PackageManager.PERMISSION_GRANTED &&
                                    ((ContextCompat.checkSelfPermission(
                                        mainActivity,
                                        Manifest.permission.ACCESS_BACKGROUND_LOCATION
                                    ) == PackageManager.PERMISSION_GRANTED
                                            ) || Build.VERSION.SDK_INT <= 29)
                                ) {
                                    Intent(
                                        ApplicationProvider.application,
                                        LocationService::class.java
                                    ).apply {
                                        action = LocationService.ACTION_START
                                        ApplicationProvider.application.startService(this)
                                    }
                                } else {
                                    viewModel.onEvent(DashboardEvent.ToggleTracking(false))
                                    multiplePermissionResultLauncher.launch(permissionsToRequest)
                                }
                            } else {

                                Intent(
                                    ApplicationProvider.application,
                                    LocationService::class.java
                                ).apply {
                                    action = LocationService.ACTION_STOP
                                    ApplicationProvider.application.startService(this)
                                }
                            }
                        },
                        colors = SwitchDefaults.colors(
                            checkedThumbColor = MaterialTheme.colorScheme.primary,
                            checkedTrackColor = Color.Gray
                        )
                    )
                }

                if (viewModel.trackingActive.value) {
                    Text(
                        text = "Tracking is currently on!",
                        color = Color.Green,
                        style = MaterialTheme.typography.bodyMedium
                    )
                } else {
                    Text(
                        text = "Tracking is currently off!",
                        color = Color.Red,
                        style = MaterialTheme.typography.bodyMedium
                    )
                }
            }
            Spacer(modifier = Modifier.height(20.dp))
            Box(
                Modifier
                    .fillMaxWidth()
                    .padding(20.dp, 0.dp, 20.dp, 0.dp)
            ) {
                Text(text = "Privacy Leak:", style = MaterialTheme.typography.headlineSmall)
            }
            Box(
                modifier = Modifier
                    .padding(10.dp, 5.dp, 10.dp, 15.dp)
                    .fillMaxWidth()
                    .height(300.dp)
                //.background(Color.Black)
            ) {
                if (viewModel.isLoading.value) {
                    CircularProgressIndicator(modifier = Modifier.align(Alignment.Center))
                } else {
                    LineChartV2(
                        data = viewModel.privacyLeakDataAbsolut,
                        modifier = Modifier
                            .fillMaxSize()
                            .padding(10.dp)
                            .align(Alignment.Center)
                    )
                    LineChartV2YAxisRight(
                        data = viewModel.privacyLeakDataScore,
                        modifier = Modifier
                            .fillMaxSize()
                            .padding(10.dp)
                            .align(Alignment.Center)
                    )
                }
            }
            Spacer(modifier = Modifier.height(20.dp))
            Box(
                Modifier
                    .fillMaxWidth()
                    .padding(20.dp, 0.dp, 20.dp, 0.dp)
            ) {
                Text(text = "Top 5 last 24 hours:", style = MaterialTheme.typography.headlineSmall)
            }
            Column(modifier = Modifier.fillMaxSize()) {
                (viewModel.top5Apps).forEach { app ->
                    AppItem(
                        app = app,
                        cumulativeUsage = viewModel.cumulativeUsage,
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(10.dp, 10.dp, 10.dp, 0.dp)
                            .clickable {
                                navController.navigate(NavigationItem.AppDetails.route + "/${app.packageName}")
                            }
                    )
                }
            }
        }
    }


    dialogQueue
        .reversed()
        .forEach { permission ->
            fun openAppSettings() {
                val intent = Intent(
                    Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                    Uri.fromParts("package", ApplicationProvider.application.packageName, null)
                )
                mainActivity.startActivity(intent)
            }
            PermissionDialog(
                permissionTextProvider = when (permission) {
                    Manifest.permission.ACCESS_FINE_LOCATION -> {
                        LocationPermissionTextProvider()
                    }

                    Manifest.permission.ACCESS_BACKGROUND_LOCATION -> {
                        BackgroundLocationPermissionTextProvider()
                    }

                    Manifest.permission.POST_NOTIFICATIONS -> {
                        NotificationPermissionTextProvider()
                    }

                    else -> return@forEach
                },
                isPermanentlyDeclined = !shouldShowRequestPermissionRationale(
                    mainActivity,
                    permission
                ),
                onDismiss = viewModel::dismissDialog,
                onOkClick = {
                    multiplePermissionResultLauncher.launch(
                        arrayOf(permission)
                    )
                    viewModel.dismissDialog()
                },
                onGoToAppSettingsClick = {
                    openAppSettings()
                    viewModel.dismissDialog()
                }
            )
        }

}


